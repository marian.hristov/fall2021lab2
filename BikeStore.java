// Marian, Hristov - 2033348

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] store = new Bicycle[4];
        store[0] = new Bicycle("Palermo", 6, 50);
        store[1] = new Bicycle("Louis-Garneau", 7, 60);
        store[2] = new Bicycle("Peugot", 8, 55);
        store[3] = new Bicycle("Ferrari", 3, 40);

        for (Bicycle bicycle : store) {
            System.out.println(bicycle);
        }
    }
}