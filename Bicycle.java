// Marian, Hristov - 2033348
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    @Override
    public String toString() {
        return "Manufacturer: " + getManufacturer() + ", Number of gears: " + getNumberGears() + ", Max speed: "
                + getMaxSpeed();
    }
}